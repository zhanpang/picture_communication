str = 'hello world'
print 1, str.replace('world', 'zhanpang')
strc = 'app hello'
print 2, strc.startswith('app')
print 3,len(strc)
print 4,'-'.join(['a','b',strc])
def demo_operation():
    # type: () -> object
    print 5,1+2,5*3,9-8
    print 6,True,not True
    print 7,5,5&3
def demo_buildinfunction():
    print 8, max(5,6)
    print 9,type(str)
    print 10,abs(-234)
    print 11,range(1,100,33)
    print 12,dir(list)
if __name__=='__main__':
    print 'hello zhanpang'
    demo_operation()
    demo_buildinfunction()

